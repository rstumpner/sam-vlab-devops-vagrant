# SAM vLAB Devops Vagrant

Ist ein Repository für eine Laborübung basierend auf Virtuellen Technologien .

Als Basis dient meist eine Aktuelle Ubuntu Distribution in einer Virtuellen Umgebung bevorzugt ist Virtualbox und VMware Play sowie Workstation , sollte aber auch unter anderen Umgebungen wenig Probleme bereiten.

* Die Einführung und Angaben für die Übung ist unter Workshop zu finden
* Hilfe zum Setup der Übung ist unter vlab zu finden

Have Fun and learn something new.




# SAM vLab with for Ansible
This is a Gitlab Repository to do an easy Virtual Lab Environment for Vagrant (https://www.vagrantup.com/) from Hashicorp ( https://www.hashicorp.com/ ) Open Source Software.

To Follow the Instrutions of this Virtual Lab:
Open your Browser

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vlab-devops-vagrant/master?grs=gitlab&t=simple

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-devops-vagrant
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Virtual Lab Environment Setup:

Requirements:
  * 3 GB Memory (minimal)
  * 2 x CPU Cores
  * Virtualbox

vLAB Setup:
  * client (Ubuntu 16.04 )
  * ansible (Ubuntu 16.04 with Ansible installed)

Option 1 Vagrant (https://www.vagrantup.com/) (local):
  * Downlad and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb?_ga=2.85169500.497796412.1511295690-1446073050.1511295690
      * dpkg -i vagrant_2.0.1_x86_64.deb

  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-devops-ansible
  * Setup the vLAB Environment with Vagrant
    * cd sam-vlab-devops-ansible
    * cd vlab
    * cd vagrant
    * vagrant up
  * Check the vLAB Setup
    * vagrant status
  * Login to work with a Node
    * vagrant ssh
