---
marp: true
---
## Systemadministraton 2 Übung
* SAM 2 Übung
* Devops


---
# Vagrant
* Devops Vagrant
* Version 20200323

---
## Agenda Vagrant
* Voraussetzungen vLAB
* Einführung 
* Umgebung vLAB
* Installation
* First Steps
* Aufgaben
* Troubleshooting
* Cheatsheet
* Links
* Notizen

---
## Voraussetzungen vLAB
* 2 x CPU Cores
* 4 GB RAM
* 2 GB Disk
* Linux (Ubuntu 18.04 / Windows 10)
* VM Virtualbox / HyperV

---
## Vorbereitungen vLAB Setup (Linux)
* Ubuntu 18.04 LTS Basis
* apt-get update
* sudo apt-get install virtualbox (6.1)
* sudo apt-get install vagrant (optional)

---
## Vorbereitungen vLAB Setup (Windows)
* Windows 10
* Installation Virtualbox 6.1 (https://www.virtualbox.org/)
* Installation Vagrant (https://www.vagrantup.com/)
  * https://releases.hashicorp.com/vagrant/

---
## Vagrant Einführung
#### Ist ein Framework um Virtuelle Umgebungen automatisiert zu erstellen und zu Verwalten

---
## Vagrant Überblick

![Vagrant Summary](_images/vagrant-sum.png)

---
## Vagrant Komponenten

* Image Repository (Boxes)
* Vagrant (Ruby)
* Provider API (VirtualBox)
* Provisioner (OS / Shell)
* Stichwort Infrastruktur as Code

---
## Installation with vLAB (optional)
#### Achtung Nested Virtualisation
* git clone https://gitlab.com/rstumpner/sam-vlab-devops-vagrant
* cd vlab/vagrant-virtualbox
* Start vLAB
  * vagrant up 
* Connect to vLAB
  * vagrant ssh


---
## First Steps
#### Check Versions

* Check Virtualbox Version:
```
vboxmanage --version
```

* Check Vagrant Version:
```
	vagrant --version
```

---
## First Steps in Vagrant
#### Create VM

Create Directory
```
  mkdir vlab/my-first-vagrant
```
Create Vagrantfile
```
  cd vlab/my-first-vagrant
  vagrant init
```

---
## Check Vagrant Base Box
#### Edit Vagrantfile

vi Vagrantfile
```
config.vm.box = "base"
```

Vagrantfile
```
config.vm.box = "ubuntu/xenial64"
```

---
## Vagrant Basebox Startup
* Starte erste Vagrant VM
```
  vagrant up
```
* Verbinde dich mit der VM über Vagrant
```
  vagrant ssh
```

---
## Vagrant Basebox Update
```
sudo apt-get update
```

---
## Vagrant Aufgaben Level 1
#### VM Parameter with Vagrant

* VM Memory
* Network

---
## Vagrant VM Parameter
* Edit Vagrantfile
```
config.vm.provider "virtualbox" do |vb|
#   # Display the VirtualBox GUI when booting the machine
#   vb.gui = true
#
#   # Customize the amount of memory on the VM:
    vb.memory = "2096"
  end
```

---
## Vagrant Network
* Edit Vagrantfile
```
config.vm.network "forwarded_port", guest: 80, host: 8085
```

---
## Vagrant Network
```
vagrnat halt
vagrant up
```

---
## Check Network Forward
http://localhost:8085/

---
## Vagrant Aufgaben Level 2
* Installation LA(M)P Umgebung

---
## Webserver Installation (Apache2)

```
sudo apt-get install apache2
```

---
## Check Apache2 Install

```
telnet localhost 80
get
```

---
## Install PHP 7

```
sudo apt-get install php7.0
sudo apt-get install libapache2-mod-php7.0
echo "<?php phpinfo(); ?>" > /var/www/html/index.php
```

---
## Check PHP7 Install
http://localhost:8085/index.php

---
## Vagrant Aufgaben Level 3
* Installation Webapplikation Kanboard

---
## Install APP (Kanboard)
#### https://www.kanboard.org
#### https://docs.kanboard.org/en/latest/admin_guide/installation.html

```
sudo apt-get install wget

wget https://github.com/kanboard/kanboard/archive/v1.2.9.zip

sudo apt-get install unzip
unzip v1.2.9.zip
chmod a+w kanboard/data/
sudo apt-get install php7.0-cli php7.0-mbstring php7.0-sqlite3 php7.0-opcache php7.0-json php7.0-mysql php7.0-pgsql php7.0-ldap php7.0-gd php7.0-xml

```

---
## Vagrant Aufgaben Level 4
* Installation Webapplikation Kanboard mit Vagrant Provisioner

---
## Vagrant Provisioner
* Nach der Basis Vagrant Box Installation kann noch ein Script ausgeführt werden diese werden unter Vagrant Provisioner genannt.
* Unterstützt werden Shell / Ansible / Puppet und Salt
* Als Beispiel eine Shell:

```
config.vm.provision "shell", inline: <<- SHELL
  apt-get update
SHELL
```

---
## Vagrant Aufgaben Level 5
* Vagrant Box Import / Export
* Multi-VM Deployment
* Provider

---
## Provider Ansible

Vagrantfile Config:

```
config.vm.provision "ansible_local" do |ansible|
 ansible.playbook = "playbook.yml"
end
```

---
## Provider Ansible

playbook-vagrant.yml:

```yml
- hosts: all
  become: yes

  tasks:
   - name: "Update APT cache"
     apt: update_cache=yes

   - name: Install common packages
     apt: name={{ item }} state=present
     with_items:
      - vim
      - wget
      - curl
      - zip
      - unzip      
      - sudo
```

---
## Vagrant Box Import / Export
#### Export:
vagrant package --base mybox  

#### Import:
vagrant box add --name mybox ~/Download/mybox.box

---
## Vagrant Multi-VM Deployment
```
$script_sqlite = <<SCRIPT
apt-get update
apt-get install -y apache2 php5 php5-sqlite php5-xdebug
apt-get clean -y
echo "ServerName localhost" >> /etc/apache2/apache2.conf
service apache2 restart
rm -f /var/www/html/index.html
date > /etc/vagrant_provisioned_at
SCRIPT

Vagrant.configure("2") do |config|

  config.vm.define "sqlite" do |m|
    m.vm.box = "ubuntu/trusty64"
    m.vm.provision "shell", inline: $script_sqlite
    m.vm.synced_folder ".", "/var/www/html", owner: "www-data", group: "www-data"
  end

  config.vm.define "mysql" do |m|
    m.vm.box = "ubuntu/trusty64"
    m.vm.provision "shell", inline: $script_mysql
    m.vm.synced_folder ".", "/var/www/html", owner: "www-data", group: "www-data"
  end


#  config.vm.network :forwarded_port, guest: 80, host: 8003
  #config.vm.network "public_network", :bridge => "en0: Wi-Fi (AirPort)"
end
```

---
## Troubleshooting
* Troubleshooting Virtualbox and Hyper-V
* Troubleshooting Vagrant mit HyperV

---
## Troubleshooting Virtualbox and Hyper-V
#### Um Vagrant mit VirtualBox und Installiertem Hyper-V zu betreiben ist es notwendig Hyper-V zu deaktivieren.

Powershell:
```
Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
```

---
## Troubleshooting Vagrant mit HyperV

Vagrant add Prvoder Manuell

```
Beispiel:
vagrant up --provider=hyperv
```

---
## Vagrant Cheat Sheet
* Vagrant Version ( `vagrant --version`)
* Create Vagrant Default File (`vagrant init`)
* Start Vagrant VM ( `vagrant up`)
* Stop Vagrant VM (`vagrnat halt`)
* Redeploy Vagrant VM (`vagrant provision`)
* delete Vagrant VM (`vagrant destroy`)
* Status of the Vagrant VM (`vagrant status`)

---
## Links:
* Vagrant Webseite
  * https://www.vagrantup.com
* Vagrant Boxes
  * https://app.vagrantup.com/boxes/search


---
## Notizen:
